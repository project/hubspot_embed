CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Hubspot Embed gives you the ability to add Hubspot Embed codes and render them.

You can place them in a Hubspot Embed Block, a text field with the Hubpsot Embed
field formatter, or you can use the Hubspot Embed ckeditor button to embed
inline.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/hubspot_embed

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/hubspot_embed


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Hubspot Embed module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Structure > Block layout and place a block.
    3. Select the "Hubspot Embed" block and place block.
    4. Paste the Hubspot Embed code, select the content types to display, pages,
       roles, and region. Save block.
    5. You can place them in a Hubspot Embed Block, a text field with the
       Hubpsot Embed field formatter, or you can use the Hubspot Embed ckeditor
       button to embed inline.


MAINTAINERS
-----------

 * Wesley Sandra (weseze) - https://www.drupal.org/u/weseze
